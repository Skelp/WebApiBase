﻿using Skelp.WebApi.Base.Interfaces;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Skelp.WebApi.Base
{
    public class DataClient : IDataClient
    {
        private HttpClient httpClient;

        public DataClient(IUserAgent userAgent, IDataClientConfig dataClientConfig)
        {
            httpClient = new HttpClient();
            var ua = userAgent.Construct(
                dataClientConfig.ProjectFriendlyName,
                dataClientConfig.AssemblyVersion,
                dataClientConfig.ProjectUrl,
                dataClientConfig.AssemblyName,
                dataClientConfig.TargetFramework);
            var success = httpClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", ua);

            if (success == false)
            {
                // TODO Throw a more descriptive exception
                throw new Exception();
            }
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }

        public async Task<Response<string>> GetAsync(Uri request, CancellationToken cancellationToken = default)
        {
            var httpResponseMsg =
                cancellationToken == default ?
                await httpClient.GetAsync(request)
                : await httpClient.GetAsync(request, cancellationToken);

            return new Response<string>(httpResponseMsg.StatusCode, httpResponseMsg.IsSuccessStatusCode,
                await httpResponseMsg.Content.ReadAsStringAsync());
        }
    }
}
