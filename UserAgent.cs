﻿using Skelp.WebApi.Base.Interfaces;
using System;

namespace Skelp.WebApi.Base
{
    public class UserAgent : IUserAgent
    {
        public string Construct(string agentName, string assemblyVersion, Uri projectUrl, string assemblyName, string targetFramework)
        {
            return
#if DEBUG
       $"C# DEBUG {agentName}/{assemblyVersion} +{projectUrl} {assemblyName}/{targetFramework}";
#else
        $"C# {agentName}/{assemblyVersion} +{projectUrl} {assemblyName}/{targetFramework}";
#endif
        }
    }
}
