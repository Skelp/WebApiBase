﻿using System;

namespace Skelp.WebApi.Base.Interfaces
{
    public interface IDataClientConfig
    {
        string AssemblyName { get; }

        string AssemblyVersion { get; }

        string ProjectFriendlyName { get; }

        Uri ProjectUrl { get; }

        string TargetFramework { get; }
    }
}
