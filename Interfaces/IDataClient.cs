﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Skelp.WebApi.Base.Interfaces
{
    public interface IDataClient : IDisposable
    {
        Task<Response<string>> GetAsync(Uri request, CancellationToken cancellationToken = default);
    }
}
