﻿namespace Skelp.WebApi.Base.Interfaces
{
    public interface IJsonMapper<T>
    {
        T Map(string json);
    }
}
