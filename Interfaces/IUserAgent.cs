﻿using System;

namespace Skelp.WebApi.Base.Interfaces
{
    public interface IUserAgent
    {
        string Construct(string agentName, string assemblyVersion, Uri repository, string assemblyName, string targetFramework);
    }
}
