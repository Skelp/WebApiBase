﻿using System.Net;

namespace Skelp.WebApi.Base
{
    public class Response<T>
    {
        public Response(HttpStatusCode statusCode, bool isSuccessStatusCode, T successObject)
        {
            StatusCode = statusCode;
            ResponseObject = successObject;
            IsSuccessStatusCode = isSuccessStatusCode;
        }

        public bool IsSuccessStatusCode { get; internal set; }

        public T ResponseObject { get; internal set; }

        public HttpStatusCode StatusCode { get; internal set; }
    }
}
