﻿using Newtonsoft.Json;
using Skelp.WebApi.Base.Interfaces;
using Skelp.WebApi.Base.Models;

namespace Skelp.WebApi.Base.Mappers
{
    public class ResponseFailureMapper : IJsonMapper<ResponseFailure>
    {
        public ResponseFailure Map(string json)
        {
            dynamic o = JsonConvert.DeserializeObject(json);
            var result = new ResponseFailure()
            {
                StatusCode = o.error.code,
                Message = o.error.message
            };

            return result;
        }
    }
}
