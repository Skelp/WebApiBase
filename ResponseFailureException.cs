﻿using Skelp.WebApi.Base.Models;
using System;
using System.Runtime.Serialization;

namespace Skelp.WebApi.Base
{
    [Serializable]
    public class ResponseFailureException : Exception
    {
        public ResponseFailureException()
        {
        }

        public ResponseFailureException(ResponseFailure responseFailure)
        {
            ResponseFailure = responseFailure;
        }

        public ResponseFailureException(string message) : base(message)
        {
        }

        public ResponseFailureException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ResponseFailureException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ResponseFailure ResponseFailure { get; private set; }
    }
}
