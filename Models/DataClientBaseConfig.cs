﻿using Skelp.WebApi.Base.Interfaces;
using System;
using System.Reflection;
using System.Runtime.Versioning;

namespace Skelp.WebApi.Base.Models
{
    public abstract class DataClientBaseConfig : IDataClientConfig
    {
        public abstract string AssemblyName { get; }

        public abstract string AssemblyVersion { get; }

        public abstract string ProjectFriendlyName { get; }

        public abstract Uri ProjectUrl { get; }

        string IDataClientConfig.TargetFramework => ((TargetFrameworkAttribute)Assembly.GetExecutingAssembly()
                    .GetCustomAttributes(typeof(TargetFrameworkAttribute), false)[0]).FrameworkName;
    }
}
