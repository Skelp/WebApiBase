﻿using System.Net;

namespace Skelp.WebApi.Base.Models
{
    public class ResponseFailure
    {
        public string Message { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}
